class: center, middle

# T-8 -- Thou shall not pass

.middle.center[[__Giovanni Ciatto__](mailto:giovanni.ciatto@unibo.it)]

.middle.center[and]

.middle.center[[Andrea Omicini](mailto:andrea.omicini@unibo.it)]

---

layout: true

## Motivation

---

### On the one side:

- Authentication, authorization, __access control__, and, more in general, cyber __security__ are very important aspects within real world's distributed systems

<!-- pause -->

- Cryptographic, one-way __hash__ functions represent a fundamental brick in several sorts of (distributed) systems.
They can be be used for access control or many other purposes
    * e.g. Byzantine Fault Tolerance
    * e.g. State Machine Replication
    * e.g. Blockchains

---

### On the other side:

<!-- pause -->

- ReST is a great architectural style, but it falls short when clients may need to be __notified__ by the server as soon as some event of interest occurs
    * e.g. clients may be interested in __chat messages__ published on a given _chat-room_ by other clients 

<!-- pause -->

- According to the ReST principles, clients should periodically __poll__ the server

<!-- pause -->

- __"Web Socket"__ is novel protocol letting web-clients and web-server interact in a __connection-ful__ way. It is commonly adopted within nowadays interactive web-based applications

---

layout: false

## Lecture goals

- Understand the basic functioning of __one-way hash functions__

<!-- pause -->

- Understand how hash functions can be used to realise __Message Authentication Codes__ (MACs)

<!-- pause -->

- Understand how MACs can be used to secure __authentication tokens__

<!-- pause -->

- Understand how users' __passwords__ are securely stored within a real (distributed) system

<!-- pause -->

- Understand how __Web Sockets__ work, why they are useful, and how the system design/implementation changes with them 

<!-- pause -->

- Practice with all the aspects above :)

---

## Cryptographic foundamentals

- We will introduce the notion of _one-way hash function_, i.e. a __cryptographic primitive__ serving multiple purposes within distributed systems

<!-- pause -->

- For instance, we will use it to implement the _Message Authentication Code_ (MAC) __cryptographic schema__

    * ... which in turns is used to __secure__ the _access tokens_ possibly exchanged by clients and servers

<!-- pause -->

- Furthermore, we will use it to __secure__ the way _passwords_ are stored within web services

<!-- pause -->

- Finally, it will be extensively used within the scope of __Blockchain technologies__ ;)


---

layout: true

### One-way Hash Functions

.fullWidth[![One way hash function, simplified representation](./hash.svg)]

.center[`Digest =` __`hash`__`(Input)`]

---

<!-- pause -->

#### Properties

<!-- pause -->

- **_Variable-size_** byte string as input, a.k.a. the __`input`__

<!-- pause -->

- __*Fixed-size*__ byte string as output, a.k.a. the __`digest`__

<!-- pause -->

- It is computationally very __simple__ to compute the digest given the input

    * this is why we talk about __one way__ hash functions

<!-- pause -->

- It is computationally __infeasible__ to compute the input given the digest

<!-- pause -->

- __Collisions__ _do exist_ and cannot be avoided

    * because the set of inputs is __infinite__, while the set of digests is __finite__

---

#### More properties

- It is computationally __infeasible__ to find a couple of collisions
    * i.e., `X` and `Y` such that __`X != Y  &&  hash(X) == Hash(Y)`__

<!-- pause -->

- It is computationally __infeasible__ to find a collision given a particular input
    * i.e., `Y` such that _`X != Y  &&  hash(X) == Hash(Y)`_, __given `X`__

<!-- pause -->

- It can be used for revealing __tampering attempts__, e.g. in Bash:
```bash
echo "Alice -> Bob: 1€" | sha256sum
    # 935c127f166469c9457e281d8051d05e072fedfb5649f9c8dde2f59357f89c89
echo "Allce -> Bob: 1€" | sha256sum
    # 9fadee9e87e51af08d0ecbeff749ce224b84b7298c9919b207546d8ff7db0bba
```

---

#### Most famous hash functions

- Algorithms from the __SHA-2__ (Secure Hash Function 2) family:
    * Four output sizes (bits): SHA-224, SHA-256, SHA-384, SHA-512

<!-- pause -->

- Algorithms from the __SHA-3__ (Secure Hash Function or Keccak) family:
    * Four output sizes (bits): KEC-224, KEC-256, KEC-384, KEC-512

<!-- pause -->

- MD5 checksum (__now considered insecure__)

<!-- pause -->

- More [here](https://en.wikipedia.org/wiki/Cryptographic_hash_function)

---

layout: true

### Message Autentication Code (MAC)

.center.threeQuartersWidth[![Message Authentication Code (MAC) through one-way hash function + secret](./mac.png)]

---

- Two trusted __parties__ want to communicate being sure their messages are __untampered__

<!-- pause -->

- They both agree on some __secret__ key __`K`__ and some hash function __hash(...)__

<!-- pause -->

- The _sender_ of payload __`M`__ always sends (`+` denotes string concatenation):

.center[__`M + C`__, where __`C = hash(K + M + K)`__ is the MAC]

<!-- pause -->

- The _receiver_ accepts a message __`M + S'`__ if and only if:

.center[__`C' == hash(K + M + K)`__]

<!-- pause -->

- If the condition is false, the message __has been altered__ while in transit

---

layout: false

### HMAC, a particular sort of MAC

The main idea is to use a more complex schema than `hash(K + M + K)`, e.g.:

.center[__`hmac`__`(K, M, hash)` `=` __`hash(K + hash(K + M))`__]

<!-- pause -->

- The __complete__ schema is presented described within the [RFC2104](https://tools.ietf.org/html/rfc2104):

.center.threeQuartersWidth[![The complete HMAC schema](./hmac.png)]

<!-- pause -->

* We __won't__ implement the __complete__ schema

<!-- pause -->

- This is the actual schema employed e.g. in __Web Tokens__
    * which, in turns, are needed for __authentication__

---

layout: true

### Tokens -- A simple schema for __authentication__

Assuming an __Auth__ server and a __Resource__ server share some secret __K__: 

.center.halfWidth[![The complete HMAC schema](./token.png)]

---

- _Clients_ can explictly __request a token__ to the _Auth server_
    * by providing their correct __credentials__ through some __secure channel__
    * e.g. __HTTPS__

<!-- pause -->

- The _Authentication server_ produces & returns a __token+MAC as response__
    * the __token__ carries the user __identity__ and __rights__
    * the __MAC__ is produced by the _Auth server_ by means of __`K`__

<!-- pause -->

- Clients must include the __token+MAC__ in _any_ request towards the _Resource server_ 
    * e.g. within the __`Authorization`__ header of an __HTTP request__

---

- __Authentication__ on the _Resource server_ implies __validating__ the token 
    * i.e. ensuring the __MAC__ matches the token __content__, through __`K`__
    * if so, the _Resource server_ can __trust__ the __identity & rights__ carried by the token

<!-- pause -->

- __Authorization__ on the _Resource server_ simply implies looking at the __identity & rights__ carried by the token
    * if the token is __untampered__ they can be trusted

<!-- pause -->

- Notice that the _Auth server_ and the _Resource server_ may actually __coincide__

---

layout: true

### JSON Web Token (JWT), a particular sort of Token

---

.middle.fullWidth[![An example of JWT](./jwt.png)]

<!-- pause -->

- Defined within the [RFC 7519](https://tools.ietf.org/html/rfc7519), currently a _Proposed Standard_

<!-- pause -->

- Main site with interactive __debugger__: https://jwt.io/
    * Play with it! :)

---

- It consists of 3 __dot__-separated chunks, each one consisting of a __base64url__-encoded string

    0. The __header__, carrying some standard metadata
    0. The __payload__, carrying some standard fields + custom information
    0. The __signature__ (i.e., the MAC)

<!-- pause -->

- The __header__ and the __payload__ are indeed __base64url__-encoded JSON objects

<!-- pause -->

- The __signature__ is actually: __`hmac`__`(K,` __`Header + '.' + Payload`__`, hash)`
    * where `K` is a secret and `hash` is some one-way hash function

<!-- pause -->

- The signature chunk is actually __optional__
    * of course, without __signatures__, JWT are highly insecure

---

#### The Header's parameters

According to [RFC2104](https://tools.ietf.org/html/rfc2104), a JWT's __header__ should contain the following __parameters__ (i.e. fields):

<!-- pause -->

- __`typ`__ (i.e. __type__) \[_optional_\] specifies the MIME type of the current JWT
    * common values: `JWT`, `jwt`, or `application/jwt`

<!-- pause -->

- __`alg`__ (i.e. __algorithm__) \[_mandatory_\] specifies the MAC algorithm adopted to produce the 
signature

    * this is needed in order for the _Resource_ server to __verify__ the token
<!-- pause -->
    * common values:
<!-- pause -->
        + `none`, in case no MAC is provided
<!-- pause -->
        + __`HS256`__, in case HMAC is employed along with SHA-256 
<!-- pause -->
        * `HS512`, in case HMAC is employed along with SHA-512
<!-- pause -->
        * `ES256`, in case the Elliptic Curve Digital Signature Algorithm (ECDSA) is employed along with SHA-256

---

#### The Payload's claims (pt 1)

According to [RFC2104](https://tools.ietf.org/html/rfc2104), a JWT's __payload__ should contain the following __claims__ (i.e. fields):

- __`iss`__ (i.e. __issuer__) \[_optional_\] specifies the URI of the entity __producing__ the JWT (i.e. the _Auth server_)

<!-- pause -->

- __`sub`__ (i.e. __subject__) \[_optional_\] specifies the URI of the entity __described__  by the JWT (i.e. the client)

<!-- pause -->

- __`aud`__ (i.e. __audience__) \[_optional_\] specifies a list of URIs identifying the intended receivers of the JWT
    * If the entity processing the JWT does not identify itself with a value in the
   `aud` claim when this claim is present, then the JWT __must__ be rejected

<!-- pause -->

- __`exp`__ (i.e. __expiration time__) \[_optional_\] specifies the expiration time on
   or after which the JWT __must not__ be accepted for processing

---

#### The Payload's claims (pt 2)

- __`nbf`__ (i.e. __not before__) \[_optional_\] specifies the time before which the JWT
   __must not__ be accepted for processing

<!-- pause -->

- __`iat`__ (i.e. __issued at__) \[_optional_\] specifies the time at which the JWT was
   created
    * It can be used to determine the age of the JWT

<!-- pause -->

- __`jid`__ (i.e. __JWT ID__) \[_optional_\] specifies a unique identifier for the JWT
    * e.g. a UUID

<!-- pause -->

- Notice that other __custom fields__ may be arbitrarily included within a JWT and secured by means of the signature
    * their values may be, in turns, JSON Objects or Arrays
    * e.g. a `User` object carrying some user's ID and __role__

---

#### Example

.middle.fullWidth[![A JWT carrying some user's information](./my-jwt.png)]

---

layout: true

### Storing passwords

---

- Passwords should never be stored as __clear text__ within the servers' databases
    * like we did in Lab-6

<!-- pause -->

- It is a common practice to only store the __digest__ of each password
    * produced by means of some __1-way hash function__ of choice

<!-- pause -->

- If only digests are stored, data __leaks__ does not necessarily imply __passwords__ leaks

<!-- pause -->

- This is a simple trick to implement:
    * it is sufficient to just store the password's digest upon __users' creation__

<!-- pause -->

- Similarly, upon __authentication__, the provided password must be hashed, and the so-produced __digest__ compared with the stored one

---

.center.middle.threeQuartersWidth[![Hashed passwords workflows](./password-hash.png)]

---

layout: false

## Web sockets

- HTTP is a level-7 request-response protocol leveraging on a TCP connection

<!-- pause -->

- Even if it relies on a connection-ful protocol, it is not well suited for those scenarios where several messages need to be exchanges among clients and servers
    * workarounds exist -- such as __long polling__ -- but they are tricks

<!-- pause -->

- I.e.: if the client needs to keep itself updated with some information, it must periodically __poll__ the server

<!-- pause -->

- __Web-sockets__ ([RFC 6455](https://tools.ietf.org/html/rfc6455)) overcome such limitations by letting clients and servers re-use the same TCP connection for exchanging several messages

<!-- pause -->

- They enable those use cases where notifications, messages, or events are __pushed__ by the server towards the client(s)

<!-- pause -->

- Notice that Web Sockets may be considered an anti-pattern w.r.t. ReST
    * since they introduce some __tight coupling__ between clients and servers

<!-- pause -->

- However, they are quite useful and used in practice

---

### Web Sockets, functioning overview

.center.middle.threeQuartersWidth[![An overview of the Web Sockets functioning](./ws-functioning.png)]


---

layout: true

### Web Sockets, details

---

A Web Socket __(WS)__ connection is started by __upgrading__ an HTTP request

.center.middle.fullWidth[![The Web Sockets handshake, i.e. HTTP upgrading](./ws-handshake.png)]

---

- Clients __start__ a WS connection by sending an HTTP __request__ towards an URL starting with __`ws://`__ or __`wss://`__. The request must be equipped with:
    * the __`Upgrade`__, __`Connection`__, __`Sec-WebSocket-Key`__, and __`-Version`__ headers

<!-- pause -->

- Servers __accept__ a WS connection by sending an HTTP __response__ equipped with:
    * the __`Upgrade`__, __`Connection`__, __`Sec-WebSocket-Accept`__ headers

<!-- pause -->

- If the server accepted the connection, then the underlying TCP connection is not closed. Instead we say the protocol has been __upgraded to WS__

<!-- pause -->

- The connection is used as a full-duplex communication channel letting the client and the server exchange an arbitrary amount of __frames__, i.e. binary or textual messages (e.g. JSON, YAML, or XML files) 

<!-- pause -->

- Both client-side and server-side WS APIs usually make it easy to intercept the reception of __full__ frames on a WS connection, e.g. through __callbacks__

<!-- pause -->

- The connection may be closed by either the client or the server by means of an _ad-hoc_ special frame

<!-- pause -->

- Of course, the connection may be abruptly interrupted by some problem occurring on the underlying network. __Applications should take care of this__

---

layout: true

### Web sockets in Vert.x

---

#### The client side

- On the client side, opening a WS connection is as simple as writing:

```java
// the connection request may contain normal HTTP headers
final MultiMap headers = MultiMap.caseInsensitiveMultiMap();

headers.add("Authorization", "MY_JWT_HERE");
// frames must contain yaml objects
headers.add("Accept", "application/yaml"); 

Vertx.vertx().createHttpClient()
    .websocket(8080, "localhost", "/web-chat/v1/" + path + queryString, headers, 
        ws -> {
            // handle successfull connection here
            // send/receive messages here
        });
```

<!-- pause -->

- Notice that __query params__ and __HTTP headers__ can be provided as usual along with the connection initialization request, since it is an ordinary HTTP request

<!-- pause -->

- This is useful, for instance, to provide authentication tokens or additional parameters regulating the subsequent connection

---

- Messages may be __sent__ by means of the `WebSocket::writeTextMessage` method, accepting `String`s:

```java
final ChatMessage newMessage = // ...

Vertx.vertx().createHttpClient()
    .websocket(8080, "localhost", "/web-chat/v1/" + path + queryString, headers, 
        ws -> {
            ws.writeTextMessage(newMessage.toYAMLString())

            ws.writeTextMessage("another string message")
        });
```

<!-- pause -->

- Messages may be __received__ by adding a callback (i.e. a handler) to the `WebSocket` instance, to be called as soon as a new __frame__ is received:

```java
ws ->
    ws.frameHandler(receivedFrame -> {
        if (receivedFrame.isText()) {
            final ChatMessage message = ChatMessage.parse(
                "application/yaml", receivedFrame.textData()
            );

            /* do something with message */
        }
    });
```
---

- If some termination condition is met, the client can politely close the connection by means of the `WebSocket::close` method:

```java
ws -> {
    if (/* some condition here */) {
        ws.close((short) 200); // status code
    }
}
```

<!-- pause -->

- If the client want to handle an early closing of the WS from the server, it may add another handler to the `WebSocket` instance:

```java
ws ->
    ws.closeHandler(x -> {
        // cleanup stuff here
    });
```

---

#### The server side

- Servers are expected to handle WS connection requests as __ordinary__ HTTP requests (e.g., by processing headers & query parameters)

<!-- pause -->

- Whenever the server is ready to accept the connection, it must simply __upgrade__ the protocol:

```java
// process headers and query parameters

final ServerWebSocket ws = getRoutingContext().request().upgrade();
```

---

- Once a `ServerWebSocket` instance has been created, the server can use it to send/receive messages on the WS connection:

```java
final ChatMessage lastMessage = // ...

// send an object to the client
ws.writeTextMessage(lastMessage.toYAMLString());

// receive a frame from the client
ws.frameHandler(frame -> {
    // handle the received frame here
});

// handle the client closing the WS connection
ws.closeHandler(x -> {
    // cleanup stuff here
});

// early close the connection is case of problems
ws.close((short) 400);
```

---

layout: true

## Exercises for Lab 8

---

0. __Exercise 8-0__: read the novel [Web-Chat v2 specification](https://app.swaggerhub.com/apis/PIKA-lab/Web-Chat-Sol/2.0.0) and spot the differences with lab 6

0. Clone the Lab 8 GitLab repository: https://gitlab.com/pika-lab/courses/ds/aa1819/lab-8

0. __Exercise 8-1__: use JWTs for authentication & hash functions for storing passwords

0. By reading the [Web-Chat v2 specification](https://app.swaggerhub.com/apis/PIKA-lab/Web-Chat-Sol/2.0.0) you should understand __what__ you should do. By looking for the many `TODO`s within the provided source code, you should understand __how__ to do it

0. __Exercise 8-2__: implement the __`ws://`__`localhost:8080/rooms/{chatRoomName}/messages`__`/stream`__ route letting the server __push__ novel messages towards the client by means of a Web Socket connection

0. All tests from within the `TestMainApiVerticle` class must succeed for your submission to be considered valid

0. Commit & push frequently on the `submissions/`__`name.surnameN`__ branch

---

### Hint: Tuple Streams

Notice the `it.unibo.sd1819.lab6.ts.objects.`__`TupleStream`__ class

<!-- pause -->

* can you figure out its meaning and functioning?

<!-- pause -->

* it let you read/take a __stream__ of `ObjectTuple`s from a particular `ObjectTupleSpace`

<!-- pause -->

* you can create a `TupleStream` out of an `ObjectTupleSpace` by means of two functions:

    - one function, __generating__ the `i`-th `ObjectTemplate` out of index `i`

    - the other function, __consuming__ the `i`-th read/taken `ObjectTuple`

<!-- pause -->

* the `i`-th tample is read/taken __only after__ the `(i-1)`-th tuple has been consumed

<!-- pause -->

* users must _explicitly_ stop the stream by means of the `TupleStream.`__`stop()`__ method

---

### Remark: about Tuple Spaces & Streams


- Of course, tuple spaces & tuple stream make it very easy to __wait__ for novel messages to become available

<!-- pause -->

- This is because Linda's `read` and `take` are subject to the __suspensive semantics__

<!-- pause -->

#### Question

<!-- pause -->

- Imagine you had no tuple spaces __at all__

<!-- pause -->

- Imagine you had to store meessages with some __RDBMS__ or __noSQL DB__ 

<!-- pause -->

.center[_Can you figure out how the service design, architecture, and implementation should be affected to let the server push messages to the requesting clients as soon as they are published?_]

---

layout: false

class: center, middle

# T-8 -- Thou shall not pass

.middle.center[[__Giovanni Ciatto__](mailto:giovanni.ciatto@unibo.it)]

.middle.center[and]

.middle.center[[Andrea Omicini](mailto:andrea.omicini@unibo.it)]